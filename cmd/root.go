/*
Copyright © 2022 Xudong Guo guoxudong.dev@gmail.com
*/
package cmd

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/PullRequestInc/go-gpt3"
	"gitlab.com/guoxudong.io/chatgpt-in-issue/pkg/gitlab"
	chatgpt "gitlab.com/guoxudong.io/chatgpt-in-issue/pkg/gpt"
	"io/ioutil"
	"log"
	"os"

	"github.com/spf13/cobra"
)

var rootCmd = &cobra.Command{
	Use:   "chat",
	Short: "ChatGPT in issue",
	Run: func(cmd *cobra.Command, args []string) {
		apiKey := os.Getenv("API_KEY")
		token := os.Getenv("GITLAB_API_TOKEN")
		host := os.Getenv("CI_SERVER_URL")
		g := gitlab.Config{
			Host:  host,
			Token: token,
		}
		path := os.Getenv("TRIGGER_PAYLOAD")
		if apiKey == "" || token == "" || path == "" || host == "" {
			fmt.Println("Please set env")
			return
		}
		c, err := readWebhookFile(path)
		if err != nil {
			log.Fatal(err)
		}
		if c.ObjectAttributes.Type != nil {
			fmt.Println("This is answered automatically by ChatGPT, which filters it out.")
			return
		}
		ctx := context.Background()
		client := gpt3.NewClient(apiKey)
		replay, err := chatgpt.GetResponse(client, ctx, c.ObjectAttributes.Note)
		if err != nil {
			log.Fatal(err)
		}
		err = g.GitLabCommentReplay(c.ProjectID, c.Issue.Iid, c.ObjectAttributes.DiscussionID, replay)
		if err != nil {
			log.Fatal(err)
		}
		//apiKey := os.Getenv("API_KEY")
		//ctx := context.Background()
		//client := gpt3.NewClient(apiKey)
		//chatgpt.GetResponse(client, ctx, "可以详细说明一个 GItLab CI 中 rules 关键字的用法吗？")
	},
}

func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func readWebhookFile(name string) (gitlab.CommentHook, error) {
	data, err := ioutil.ReadFile(name)
	if err != nil {
		log.Fatal(err)
	}

	// Unmarshal the data into a Person object.
	var comment gitlab.CommentHook
	if err := json.Unmarshal(data, &comment); err != nil {
		log.Fatal(err)
	}
	return comment, err
}
